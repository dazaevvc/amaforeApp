export const preguntasRespuestasPaginas = [
  {
    pregunta: 'La escasez se produce cuando ',
    respuesta:
      'las necesidades y deseos son mayores que la cantidad disponible de recursos',
    pagina: '17'
  },
  {
    pregunta: 'A la mejor opción que se dejó de lado al elegir se llama ',
    respuesta: 'costo de oportunidad',
    pagina: '41'
  },
  {
    pregunta: 'Una meta es alcanzable cuando ',
    respuesta: 'se tienen las herramientas y las personas para conseguirla',
    pagina: '74'
  },
  {
    pregunta: 'Ahorrar no es lo que nos sobra del ingreso ',
    respuesta:
      'sino establecer una cantidad fija que se debe incluir en su presupuesto',
    pagina: '88'
  },
  {
    pregunta: 'Ahorrar para el retiro es ',
    respuesta:
      'una meta de largo plazo tan importante como las metas de corto y mediano plazo',
    pagina: '127'
  }
];
